const assert = require('assert');
const app = require('../../src/app');

describe('\'statistic\' service', () => {
  it('registered the service', () => {
    const service = app.service('statistic');

    assert.ok(service, 'Registered the service');
  });
});
