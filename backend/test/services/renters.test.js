const assert = require('assert');
const app = require('../../src/app');

describe('\'renters\' service', () => {
  it('registered the service', () => {
    const service = app.service('renters');

    assert.ok(service, 'Registered the service');
  });
});
