const assert = require('assert');
const app = require('../../src/app');

describe('\'percents\' service', () => {
  it('registered the service', () => {
    const service = app.service('percents');

    assert.ok(service, 'Registered the service');
  });
});
