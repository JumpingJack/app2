const assert = require('assert');
const app = require('../../src/app');

describe('\'renters-type\' service', () => {
  it('registered the service', () => {
    const service = app.service('renters-type');

    assert.ok(service, 'Registered the service');
  });
});
