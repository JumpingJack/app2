const assert = require('assert');
const app = require('../../src/app');

describe('\'market\' service', () => {
  it('registered the service', () => {
    const service = app.service('market');

    assert.ok(service, 'Registered the service');
  });
});
