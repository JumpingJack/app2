const assert = require('assert');
const app = require('../../src/app');

describe('\'floors\' service', () => {
  it('registered the service', () => {
    const service = app.service('floors');

    assert.ok(service, 'Registered the service');
  });
});
