const logger = require('winston');

module.exports = function () {
  return hook => {

    return hook.app.service('market').find({
      query: {
        place_id: hook.data.name,
        date: hook.data.infoDate,
        floor: hook.data.floor
      }
    }).then(res => {
      const resArr = res;

      if (resArr.length) {
        hook.app.service('market').patch(resArr[0].id, {
          price_base: hook.data.price_base,
          price_ekp: hook.data.price_ekp,
          additional_pay: hook.data.additional_pay,
          price_var: hook.data.price_var,
          percentPay: hook.data.percentPay,
          market: hook.data.market,
          YEvalue: hook.data.YEvalue,
          YEuse: hook.data.YEuse,
          status: hook.data.status
        }).then(() => {
          return hook;
        })
      } else {
        hook.app.service('market').create({
          place_id: hook.data.name,
          date: hook.data.infoDate,
          price_base: hook.data.price_base,
          price_ekp: hook.data.price_ekp,
          additional_pay: hook.data.additional_pay,
          price_var: hook.data.price_var,
          percentPay: hook.data.percentPay,
          market: hook.data.market,
          floor: hook.data.floor,
          status: hook.data.status,
          YEvalue: hook.data.YEvalue,
          YEuse: hook.data.YEuse
        }).then(() => {
          return hook
        })
      }
    });
  }
};