const logger = require('winston');

module.exports = function () {
  return hook => {

    if (hook.data.name) {
      return hook.app.service('percents').find({
        query: {
          place_id: hook.data.name
        }
      }).then(res => {
        if (res.data[0]) {
          hook.result.percentPay = res.data[0].value;
          hook.result.percentPayDate = res.data[0].date;
        }

        return hook;
      });
    }
  }
};