const { authenticate } = require('@feathersjs/authentication').hooks;
//const { restrictToOwner } = require('feathers-authentication-hooks');

const {
  hashPassword, protect
} = require('@feathersjs/authentication-local').hooks;

//const restrict = [
//  authenticate('jwt'),
//  restrictToOwner({ idField: 'id', as: 'role' })
//];

module.exports = {
  before: {
    all: [],
    find: [
      // authenticate('jwt')
    ],
    get: [
      // hook => {
      //   // hook.result.userid = hook.id;
      //   console.log(hook.params.user);
      //   return hook;
      // }
    ],
    create: [ hashPassword() ],
    update: [ hashPassword() ],
    patch: [ hashPassword() ],
    remove: []
  },

  after: {
    all: [ 
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [

    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
