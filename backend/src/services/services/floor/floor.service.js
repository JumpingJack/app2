// Initializes the `floor` service on path `/floor`
const createService = require('feathers-sequelize');
const createModel = require('../../models/floor.model');
const hooks = require('./floor.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'floor',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/floor', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('floor');

  service.hooks(hooks);
};
