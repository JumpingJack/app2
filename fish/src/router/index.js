import Vue from 'vue'
import Router from 'vue-router'
import mainPage from '@/components/main-page'
import adminState from '@/components/admin-state'
import profilePage from '@/components/profilePage'
import floor from '@/components/floor'
import parking from '@/components/parking'
import parkingFloor from '@/components/parking-floor'
import tableFloor from '@/components/table-floor'
import tableParking from '@/components/table-parking'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'mainPage',
      component: mainPage
    },
    {
      path: '/profile',
      name: 'profile-page',
      component: profilePage,
      beforeEnter: guardRoute
    },
    {
      path: '/admin',
      name: 'admin-state',
      component: adminState,
      beforeEnter: guardRoute
    },
    {
      path: '/floor/:id',
      name: 'floor',
      component: floor,
      beforeEnter: guardRoute
    },
    {
      path: '/table-floor/:id',
      name: 'table-floor',
      component: tableFloor,
      beforeEnter: guardRoute
    },
    {
      path: '/table-parking/:id',
      name: 'table-parking',
      component: tableParking,
      beforeEnter: guardRoute
    },
    {
      path: '/parking',
      name: 'parking',
      component: parking,
      beforeEnter: guardRoute
    },
    {
      path: '/parking-floor/:id',
      name: 'parking-floor',
      component: parkingFloor,
      beforeEnter: guardRoute
    },
  ]
})

function guardRoute(to, from, next) {
  if (localStorage.token) {
    next()
  } else {
    next({
      name: 'mainPage'
    })
  }
}
